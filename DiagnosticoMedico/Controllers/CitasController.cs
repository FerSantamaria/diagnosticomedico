﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiagnosticoMedico.Models;

namespace DiagnosticoMedico.Controllers
{
    public class CitasController : Controller
    {
        private hospital db = new hospital();

        // GET: Citas
        public ActionResult Index()
        {
            var citas = db.Citas.Include(c => c.Medico).Include(c => c.Paciente);

            if (Session["tipo"] != null && (int)Session["tipo"] == 2)
            {
                int emp = (int)Session["idUsuario"];
                DateTime hoy = DateTime.Today;

                citas = from c in db.Citas
                        join m in db.Medicos on c.id_medico equals m.id
                        join e in db.Empleadoes on m.id_empleado equals e.id
                        where e.id.Equals(emp) && c.dia.Equals(hoy)
                        select c;
            }

            return View(citas.ToList());
        }

        // GET: Citas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cita cita = db.Citas.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            return View(cita);
        }

        // GET: Citas/Create
        public ActionResult Create()
        {
            var medicos = from m in db.Medicos
                          join e in db.Empleadoes on m.id_empleado equals e.id
                          select new { m.id, e.nombre, e.apellido };

            List<SelectListItem> listaMeds = new List<SelectListItem>();
            foreach (var item in medicos)
            {
                listaMeds.Add(new SelectListItem { Text = item.nombre + " " + item.apellido, Value = item.id.ToString() });
            }
            
            ViewBag.id_medico = listaMeds;

            var pacientes = db.Pacientes;

            List<SelectListItem> listaPacientes = new List<SelectListItem>();
            foreach (var item in pacientes)
            {
                listaPacientes.Add(new SelectListItem { Text = item.nombreCompleto, Value = item.id.ToString() });
            }

            ViewBag.id_paciente = listaPacientes;
            return View();
        }

        // POST: Citas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,dia,hora,id_paciente,id_medico")] Cita cita)
        {
            if (ModelState.IsValid)
            {
                var ocupada = (from c in db.Citas
                             where c.dia.Equals(cita.dia) && c.hora.Equals(cita.hora)
                             select c).FirstOrDefault();

                if (ocupada == null)
                {
                    db.Citas.Add(cita);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Error = "Ya existe otra cita programada a las " + cita.hora + " el día " + cita.dia.ToShortDateString();
                }
            }


            var medicos = from m in db.Medicos
                          join e in db.Empleadoes on m.id_empleado equals e.id
                          select new { m.id, e.nombre, e.apellido };

            List<SelectListItem> listaMeds = new List<SelectListItem>();
            foreach (var item in medicos)
            {
                listaMeds.Add(new SelectListItem { Text = item.nombre + " " + item.apellido, Value = item.id.ToString() });
            }

            ViewBag.id_medico = listaMeds;
            //ViewBag.id_medico = new SelectList(db.Medicos, "id", "id", cita.id_medico);
            ViewBag.id_paciente = new SelectList(db.Pacientes, "id", "nombre", cita.id_paciente);
            return View(cita);
        }

        // GET: Citas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cita cita = db.Citas.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }

            var medicos = from m in db.Medicos
                          join e in db.Empleadoes on m.id_empleado equals e.id
                          select new { m.id, e.nombre, e.apellido };

            List<SelectListItem> listaMeds = new List<SelectListItem>();
            foreach (var item in medicos)
            {
                listaMeds.Add(new SelectListItem { Text = item.nombre + " " + item.apellido, Value = item.id.ToString() });
            }

            ViewBag.id_medico = listaMeds;
            //ViewBag.id_medico = new SelectList(db.Medicos, "id", "id", cita.id_medico);
            ViewBag.id_paciente = new SelectList(db.Pacientes, "id", "nombre", cita.id_paciente);
            return View(cita);
        }

        // POST: Citas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,dia,hora,id_paciente,id_medico")] Cita cita)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cita).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_medico = new SelectList(db.Medicos, "id", "id", cita.id_medico);
            ViewBag.id_paciente = new SelectList(db.Pacientes, "id", "nombre", cita.id_paciente);
            return View(cita);
        }

        // GET: Citas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cita cita = db.Citas.Find(id);
            if (cita == null)
            {
                return HttpNotFound();
            }
            return View(cita);
        }

        // POST: Citas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cita cita = db.Citas.Find(id);
            db.Citas.Remove(cita);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
