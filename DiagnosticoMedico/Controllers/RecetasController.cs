﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiagnosticoMedico.Models;

namespace DiagnosticoMedico.Controllers
{
    public class RecetasController : Controller
    {
        private hospital db = new hospital();

        // GET: Recetas
        public ActionResult Index()
        {
            if (Session["tipo"] != null && (int)Session["tipo"] == 3) return RedirectToAction("Index", "Home");

            var recetas = db.Recetas.Include(r => r.Expediente).Include(r => r.Medicamento);
            return View(recetas.ToList());
        }

        // GET: Recetas/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["tipo"] != null && (int)Session["tipo"] == 3) return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Receta receta = db.Recetas.Find(id);
            if (receta == null)
            {
                return HttpNotFound();
            }
            return View(receta);
        }

        // GET: Recetas/Create
        public ActionResult Create()
        {
            if (Session["tipo"] != null && (int)Session["tipo"] == 3) return RedirectToAction("Index", "Home");

            ViewBag.id_expediente = new SelectList(db.Expedientes, "id", "problema");
            ViewBag.id_medicamento = new SelectList(db.Medicamentoes, "id", "nombre");
            return View();
        }

        // POST: Recetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_expediente,id_medicamento,dosis")] Receta receta)
        {
            if (ModelState.IsValid)
            {
                db.Recetas.Add(receta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_expediente = new SelectList(db.Expedientes, "id", "problema", receta.id_expediente);
            ViewBag.id_medicamento = new SelectList(db.Medicamentoes, "id", "nombre", receta.id_medicamento);
            return View(receta);
        }

        // GET: Recetas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["tipo"] != null && (int)Session["tipo"] == 3) return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Receta receta = db.Recetas.Find(id);
            if (receta == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_expediente = new SelectList(db.Expedientes, "id", "problema", receta.id_expediente);
            ViewBag.id_medicamento = new SelectList(db.Medicamentoes, "id", "nombre", receta.id_medicamento);
            return View(receta);
        }

        // POST: Recetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_expediente,id_medicamento,dosis")] Receta receta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_expediente = new SelectList(db.Expedientes, "id", "problema", receta.id_expediente);
            ViewBag.id_medicamento = new SelectList(db.Medicamentoes, "id", "nombre", receta.id_medicamento);
            return View(receta);
        }

        // GET: Recetas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["tipo"] != null && (int)Session["tipo"] == 3) return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Receta receta = db.Recetas.Find(id);
            if (receta == null)
            {
                return HttpNotFound();
            }
            return View(receta);
        }

        // POST: Recetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Receta receta = db.Recetas.Find(id);
            db.Recetas.Remove(receta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
