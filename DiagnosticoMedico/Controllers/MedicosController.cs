﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DiagnosticoMedico.Models;

namespace DiagnosticoMedico.Controllers
{
    public class MedicosController : Controller
    {
        private hospital db = new hospital();

        // GET: Medicos
        public ActionResult Index()
        {
            if (Session["tipo"] != null && (int)Session["tipo"] != 1) return RedirectToAction("Index", "Home");

            var medicos = db.Medicos.Include(m => m.Empleado).Include(m => m.Especialidad);
            return View(medicos.ToList());
        }

        // GET: Medicos/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["tipo"] != null && (int)Session["tipo"] != 1) return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // GET: Medicos/Create
        public ActionResult Create()
        {
            if (Session["tipo"] != null && (int)Session["tipo"] != 1) return RedirectToAction("Index", "Home");

            ViewBag.id_empleado = new SelectList(db.Empleadoes, "id", "nombre");
            ViewBag.id_especialidad = new SelectList(db.Especialidads, "id", "nombre");
            return View();
        }

        // POST: Medicos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_empleado,id_especialidad")] Medico medico)
        {
            if (ModelState.IsValid)
            {
                db.Medicos.Add(medico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_empleado = new SelectList(db.Empleadoes, "id", "nombre", medico.id_empleado);
            ViewBag.id_especialidad = new SelectList(db.Especialidads, "id", "nombre", medico.id_especialidad);
            return View(medico);
        }

        // GET: Medicos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Session["tipo"] != null && (int)Session["tipo"] != 1) return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_empleado = new SelectList(db.Empleadoes, "id", "nombre", medico.id_empleado);
            ViewBag.id_especialidad = new SelectList(db.Especialidads, "id", "nombre", medico.id_especialidad);
            return View(medico);
        }

        // POST: Medicos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_empleado,id_especialidad")] Medico medico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(medico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_empleado = new SelectList(db.Empleadoes, "id", "nombre", medico.id_empleado);
            ViewBag.id_especialidad = new SelectList(db.Especialidads, "id", "nombre", medico.id_especialidad);
            return View(medico);
        }

        // GET: Medicos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Session["tipo"] != null && (int)Session["tipo"] != 1) return RedirectToAction("Index", "Home");

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // POST: Medicos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Medico medico = db.Medicos.Find(id);
            db.Medicos.Remove(medico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
