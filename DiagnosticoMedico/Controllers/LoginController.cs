﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DiagnosticoMedico.Models;

namespace DiagnosticoMedico.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Empleado emp)
        {
            using(hospital db = new hospital())
            {
                var detalles = db.Empleadoes.Where(x => x.usuario == emp.usuario && x.contrasenia == emp.contrasenia).FirstOrDefault();

                if (detalles == null)
                {
                    ViewBag.Error = "Usuario y/o contraseña incorrectos!";
                    return View();
                }
                else
                {
                    Session["idUsuario"] = detalles.id;
                    Session["usuario"] = detalles.nombre + " " + detalles.apellido;
                    Session["tipo"] = detalles.tipo;

                    return RedirectToAction("Index", "Home");
                }
            }
        }

        public ActionResult Salir()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}