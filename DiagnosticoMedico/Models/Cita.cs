namespace DiagnosticoMedico.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Cita")]
    public partial class Cita
    {
        public int id { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        [Required]
        public DateTime dia { get; set; }

        [Display(Name = "Hora")]
        [DataType(DataType.Time)]
        [Required]
        public TimeSpan hora { get; set; }

        [Display(Name = "Paciente")]
        [Required]
        public int id_paciente { get; set; }

        [Display(Name = "M�dico")]
        [Required]
        public int id_medico { get; set; }

        public virtual Medico Medico { get; set; }

        public virtual Paciente Paciente { get; set; }
    }
}
