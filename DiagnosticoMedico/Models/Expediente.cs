namespace DiagnosticoMedico.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Expediente")]
    public partial class Expediente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Expediente()
        {
            Recetas = new HashSet<Receta>();
        }

        public int id { get; set; }

        public int id_paciente { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string problema { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string diagnostico { get; set; }

        public virtual Paciente Paciente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Receta> Recetas { get; set; }
    }
}
