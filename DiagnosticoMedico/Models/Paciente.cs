namespace DiagnosticoMedico.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Paciente")]
    public partial class Paciente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Paciente()
        {
            Citas = new HashSet<Cita>();
            Expedientes = new HashSet<Expediente>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string nombre { get; set; }

        [Required]
        [StringLength(50)]
        public string apellido { get; set; }

        [Required]
        [StringLength(200)]
        public string direccion { get; set; }

        [Required]
        [StringLength(8)]
        public string telefono { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime fecha_nacimiento { get; set; }

        [Required]
        public int sexo { get; set; }

        [Required]
        [StringLength(50)]
        public string ocupacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cita> Citas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Expediente> Expedientes { get; set; }

        public string nombreCompleto
        {
            get
            {
                return String.Format("{0} {1}", nombre, apellido);
            }
        }
    }
}
