namespace DiagnosticoMedico.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class hospital : DbContext
    {
        public hospital()
            : base("name=hospital")
        {
        }

        public virtual DbSet<Cita> Citas { get; set; }
        public virtual DbSet<Empleado> Empleadoes { get; set; }
        public virtual DbSet<Especialidad> Especialidads { get; set; }
        public virtual DbSet<Expediente> Expedientes { get; set; }
        public virtual DbSet<Medicamento> Medicamentoes { get; set; }
        public virtual DbSet<Medico> Medicos { get; set; }
        public virtual DbSet<Paciente> Pacientes { get; set; }
        public virtual DbSet<Receta> Recetas { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empleado>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Empleado>()
                .Property(e => e.apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Empleado>()
                .Property(e => e.usuario)
                .IsUnicode(false);

            modelBuilder.Entity<Empleado>()
                .Property(e => e.contrasenia)
                .IsUnicode(false);

            modelBuilder.Entity<Empleado>()
                .HasMany(e => e.Medicos)
                .WithRequired(e => e.Empleado)
                .HasForeignKey(e => e.id_empleado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Especialidad>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Especialidad>()
                .HasMany(e => e.Medicos)
                .WithRequired(e => e.Especialidad)
                .HasForeignKey(e => e.id_especialidad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Expediente>()
                .Property(e => e.problema)
                .IsUnicode(false);

            modelBuilder.Entity<Expediente>()
                .Property(e => e.diagnostico)
                .IsUnicode(false);

            modelBuilder.Entity<Expediente>()
                .HasMany(e => e.Recetas)
                .WithRequired(e => e.Expediente)
                .HasForeignKey(e => e.id_expediente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medicamento>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Medicamento>()
                .HasMany(e => e.Recetas)
                .WithRequired(e => e.Medicamento)
                .HasForeignKey(e => e.id_medicamento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Medico>()
                .HasMany(e => e.Citas)
                .WithRequired(e => e.Medico)
                .HasForeignKey(e => e.id_medico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .Property(e => e.ocupacion)
                .IsUnicode(false);

            modelBuilder.Entity<Paciente>()
                .HasMany(e => e.Citas)
                .WithRequired(e => e.Paciente)
                .HasForeignKey(e => e.id_paciente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paciente>()
                .HasMany(e => e.Expedientes)
                .WithRequired(e => e.Paciente)
                .HasForeignKey(e => e.id_paciente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Receta>()
                .Property(e => e.dosis)
                .IsUnicode(false);
        }
    }
}
