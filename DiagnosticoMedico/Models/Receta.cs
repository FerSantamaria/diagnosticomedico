namespace DiagnosticoMedico.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Receta")]
    public partial class Receta
    {
        public int id { get; set; }

        public int id_expediente { get; set; }

        public int id_medicamento { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string dosis { get; set; }

        public virtual Expediente Expediente { get; set; }

        public virtual Medicamento Medicamento { get; set; }
    }
}
