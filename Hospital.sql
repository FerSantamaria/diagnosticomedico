USE master
GO

CREATE DATABASE Hospital
GO

USE Hospital
GO

CREATE TABLE Paciente(
	id int IDENTITY(1,1) PRIMARY KEY,
	nombre varchar(50) NOT NULL,
	apellido varchar(50) NOT NULL,
	direccion varchar(200) NOT NULL,
	telefono varchar(8) NOT NULL,
	fecha_nacimiento date NOT NULL,
	sexo int NOT NULL,
	ocupacion varchar(50) NOT NULL
)

CREATE TABLE Especialidad(
	id int IDENTITY(1,1) PRIMARY KEY,
	nombre varchar(75) NOT NULL
)

CREATE TABLE Empleado(
	id int IDENTITY(1,1) PRIMARY KEY,
	nombre varchar(50) NOT NULL,
	apellido varchar(50) NOT NULL,
	usuario varchar(10) NOT NULL,
	contrasenia varchar(256) NOT NULL,
	tipo int NOT NULL
)

CREATE TABLE Medico(
	id int IDENTITY(1,1) PRIMARY KEY,
	id_empleado int NOT NULL,
	id_especialidad int NOT NULL,
	CONSTRAINT fk_medico_empleado FOREIGN KEY (id_empleado) REFERENCES Empleado(id),
	CONSTRAINT fk_medico_especialidad FOREIGN KEY (id_especialidad) REFERENCES Especialidad(id)
)

CREATE TABLE Cita(
	id int IDENTITY(1,1) PRIMARY KEY,
	dia date NOT NULL,
	hora time NOT NULL,
	id_paciente int NOT NULL,
	id_medico int NOT NULL,
	CONSTRAINT fk_cita_paciente FOREIGN KEY (id_paciente) REFERENCES Paciente(id),
	CONSTRAINT fk_cita_medico FOREIGN KEY (id_medico) REFERENCES Medico(id)
)

CREATE TABLE Expediente(
	id int IDENTITY(1,1) PRIMARY KEY,
	id_paciente int NOT NULL,
	problema text NOT NULL,
	diagnostico text NOT NULL,
	CONSTRAINT fk_expediente_paciente FOREIGN KEY (id_paciente) REFERENCES Paciente(id),
)

CREATE TABLE Medicamento(
	id int IDENTITY(1,1) PRIMARY KEY,
	nombre varchar(100) NOT NULL
)

CREATE TABLE Receta(
	id int IDENTITY(1,1) PRIMARY KEY,
	id_expediente int NOT NULL,
	id_medicamento int NOT NULL,
	dosis text NOT NULL,
	CONSTRAINT fk_receta_expediente FOREIGN KEY (id_expediente) REFERENCES Expediente(id),
	CONSTRAINT fk_receta_medicamento FOREIGN KEY (id_medicamento) REFERENCES Medicamento(id)
)

